export default function cacheFunction(cb) {
    const cache = {};
    return (argument) => {
        if(cache[argument]){
            return cache[argument]
        }else{
            cache[argument] = cb(argument);
            return cache[argument]
        }
        
    }
    
}

