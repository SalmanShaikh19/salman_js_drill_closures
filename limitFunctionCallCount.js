export default function limitFunctionCallCount(cb, n) {
    return (a) => {
         for(let index = 0; index < n; index++) {
             cb(a);
         }       
    }   
}
