import cacheFunction from "../cacheFunction.js";

function cb (a) {
    return(a*a);
}

const cache = cacheFunction(cb);
console.log(cache(2));
console.log(cache(2));