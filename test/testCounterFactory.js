import counterFactory from "../counterFactory.js";

const counter1 = counterFactory();
counter1.increment();
console.log(counter1.increment());

const counter2 = counterFactory();
console.log(counter2.decrement());
