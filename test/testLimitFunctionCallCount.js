import limitFunctionCallCount from "../limitFunctionCallCount.js";

function cb (a) {
    console.log(a*a);
}

const limitfunction = limitFunctionCallCount(cb,4);
limitfunction(3);